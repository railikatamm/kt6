import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /** Main method. */
    public static void main (String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /** Actual main method to run examples and everything. */
    public void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(6, 9);
        System.out.println(g);
        System.out.println("Source Vertex: " + g.first);
        //System.out.println("Destination Vertex: " + g.getVertexById("v5"));
        System.out.println("Traversed path: " + g.breadthFirst(g.first, null));

        System.out.println(g.createTriangleGraph());
        System.out.println("Complement graph:");
        System.out.println(g.getComplementGraph());

        //test1();
        //test2();
        //test3();
        //test4();
        //test5();
        //test6();
    }

    /**
     * Test to find path in simple orientated graph
     */
    public void test1(){
        Graph g1 = new Graph("G1");

        Vertex a = new Vertex("A");
        Vertex b = new Vertex("B");
        Vertex c = new Vertex("C");
        Vertex d = new Vertex("D");
        Vertex e = new Vertex("E");
        Vertex f = new Vertex("F");
        Vertex h = new Vertex("H");

        g1.first = a;
        a.next = b;
        b.next = c;
        c.next = f;
        f.next = d;
        d.next = e;
        e.next = h;

        Arc ab = new Arc("AB");
        Arc bc = new Arc("BC");
        Arc bd = new Arc("BD");
        Arc be = new Arc("BE");
        Arc de = new Arc("DE");
        Arc eh = new Arc("EH");
        Arc cf = new Arc("CF");
        Arc ce = new Arc("CE");
        Arc ha = new Arc("HA");

        bc.next = bd;
        cf.next = ce;

        a.first = ab;
        b.first = bc;
        c.first = cf;
        d.first = de;
        e.first = eh;
        h.first = ha;

        ab.target = b;
        bc.target = c;
        bd.target = d;
        be.target = e;
        de.target = e;
        eh.target = h;
        cf.target = f;
        ce.target = e;
        ha.target = a;

        System.out.println(g1);
        System.out.println("Source Vertex: " + a);
        System.out.println("Destination Vertex: " + h);
        System.out.println("Traversed path: " + g1.breadthFirst(a, h));
    }

    /**
     * Test to find path in simple non-orientated graph
     */
    public void test2(){
        Graph g2 = new Graph("G2");

        Vertex a = new Vertex("A");
        Vertex b = new Vertex("B");
        Vertex c = new Vertex("C");
        Vertex d = new Vertex("D");
        Vertex e = new Vertex("E");
        Vertex f = new Vertex("F");
        Vertex h = new Vertex("H");

        g2.first = a;
        a.next = b;
        b.next = c;
        c.next = f;
        f.next = d;
        d.next = e;
        e.next = h;

        Arc ab = new Arc("AB");
        Arc ba = new Arc("BA");
        Arc bc = new Arc("BC");
        Arc cb = new Arc("CB");
        Arc bd = new Arc("BD");
        Arc db = new Arc("DB");
        Arc be = new Arc("BE");
        Arc eb = new Arc("EB");
        Arc de = new Arc("DE");
        Arc ed = new Arc("ED");
        Arc eh = new Arc("EH");
        Arc he = new Arc("HE");
        Arc cf = new Arc("CF");
        Arc fc = new Arc("FC");
        Arc ce = new Arc("CE");
        Arc ec = new Arc("EC");
        Arc ha = new Arc("HA");
        Arc ah = new Arc("AH");

        ab.next = ah;
        bc.next = bd;
        bd.next = ba;
        cf.next = ce;
        ce.next = cb;
        de.next = db;
        eh.next = ed;
        ed.next = ec;
        ha.next = he;

        a.first = ab;
        b.first = bc;
        c.first = cf;
        d.first = de;
        e.first = eh;
        h.first = ha;
        f.first = fc;

        ab.target = b;
        ba.target = a;
        bc.target = c;
        cb.target = b;
        bd.target = d;
        db.target = b;
        be.target = e;
        eb.target = b;
        de.target = e;
        ed.target = d;
        eh.target = h;
        he.target = e;
        cf.target = f;
        fc.target = c;
        ce.target = e;
        ec.target = c;
        ha.target = a;
        ah.target = h;

        System.out.println(g2);
        System.out.println("Source Vertex: " + a);
        System.out.println("Destination Vertex: " + h);
        System.out.println("Traversed path: " + g2.breadthFirst(a, f));
    }

    /**
     * Test to find check if method throws error if source not in Graph
     */
    public void test3(){
        Graph g3 = new Graph("G");
        g3.createRandomSimpleGraph(6, 9);
        Vertex v7 = new Vertex("V7");

        System.out.println(g3);
        System.out.println("Source: " + v7);
        System.out.println("Destination: " + g3.getVertexById("v2"));
        try {
            System.out.println(g3.breadthFirst(v7, g3.getVertexById("v2")));
        } catch (Exception e){
            System.out.println(e);
        }
    }

    /**
     * Test to find check if method throws error if destination not in Graph
     */
    public void test4(){
        Graph g4 = new Graph("G");
        g4.createRandomSimpleGraph(6, 9);
        Vertex v7 = new Vertex("V7");

        System.out.println(g4);
        System.out.println("Source: " + g4.getVertexById("v1"));
        System.out.println("Destination: " + v7);
        try {
            System.out.println(g4.breadthFirst(g4.getVertexById("v1"), v7));
        } catch (Exception e){
            System.out.println(e);
        }
    }

    /**
     * Test to find check if method throws error if Graph does not contain any Vertices
     */
    public void test5(){
        Graph g5 = new Graph("G5");
        Vertex a = new Vertex("A");
        Vertex b = new Vertex("B");
        try {
            g5.breadthFirst(a, b);
        } catch (Exception e){
            System.out.println(e);
        }
    }

    /**
     * Test that measures time it takes to traverse a large (2000+ Vertices) Graph
     */
    public void test6(){
        Graph g6 = new Graph("G6");
        g6.createRandomSimpleGraph(2000, 2000);
        long stime, ftime, diff;
        stime = System.nanoTime();
        List<Arc> arcs = g6.breadthFirst(g6.first, null);
        ftime = System.nanoTime();
        diff = ftime - stime;
        System.out.println("Time: " + diff / 1000000 + "ms");
        System.out.println("Traversed " + arcs.size() + " arcs");
    }

    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        private List<Integer> vertexes;

        Vertex (String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex (String s) {
            this (s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        public void setInfo(int a){
            info = a;
        }

        public int getInfo(){
            return info;
        }

        public String getId(){
            return id;
        }

        /**
         * Method returns all arcs connected to Vertex
         * @return List<Arc>
         */
        public List<Arc> getArcs(){
            List<Arc> connectedArcs = new LinkedList<>();

            if (first == null){
                return connectedArcs;
            }

            Arc current = first;
            connectedArcs.add(first);

            while (true) {
                if (current.next != null){
                    current = current.next;
                    connectedArcs.add(current);
                } else {
                    break;
                }
            }

            return connectedArcs;
        }
    }


    /** Arc represents one arrow in the graph. Two-directional edges are"
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;

        Arc (String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc (String s) {
            this (s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        public Vertex getTargetVertex(){
            return target;
        }
    }

    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;

        Graph (String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph (String s) {
            this (s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty ("line.separator");
            StringBuffer sb = new StringBuffer (nl);
            sb.append (id);
            sb.append (nl);
            Vertex v = first;
            while (v != null) {
                sb.append (v.toString());
                sb.append (" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append (" ");
                    sb.append (a.toString());
                    sb.append (" (");
                    sb.append (v.toString());
                    sb.append ("->");
                    sb.append (a.target.toString());
                    sb.append (")");
                    a = a.next;
                }
                sb.append (nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex (String vid) {
            Vertex res = new Vertex (vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc (String aid, Vertex from, Vertex to) {
            Arc res = new Arc (aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         * @param n number of vertices added to this graph
         */
        public void createRandomTree (int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex [n];
            for (int i = 0; i < n; i++) {
                varray [i] = createVertex ("v" + String.valueOf(n-i));
                if (i > 0) {
                    int vnr = (int)(Math.random()*i);
                    createArc ("a" + varray [vnr].toString() + "_"
                            + varray [i].toString(), varray [vnr], varray [i]);
                    createArc ("a" + varray [i].toString() + "_"
                            + varray [vnr].toString(), varray [i], varray [vnr]);
                } else {}
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int [info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res [i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph (int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException ("Too many vertices: " + n);
            if (m < n-1 || m > n*(n-1)/2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree (n);       // n-1 edges created here
            Vertex[] vert = new Vertex [n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int)(Math.random()*n);  // random source
                int j = (int)(Math.random()*n);  // random target
                if (i==j)
                    continue;  // no loops
                if (connected [i][j] != 0 || connected [j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert [i];
                Vertex vj = vert [j];
                createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected [i][j] = 1;
                createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected [j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Returns a list of vertices from current graph
         */
        public List<Vertex> getVertexList(){
            List<Vertex> vertices = new LinkedList<>();

            Vertex v = first;
            while (v != null){
                vertices.add(v);
                v = v.next;
            }

            return vertices;
        }

        /**
         * Returns Vertex in graph if found. Returns null, if not found.
         * @param id vertex id
         * @return Vertex
         */
        public Vertex getVertexById(String id){
            for (Vertex vertex : getVertexList()) {
                if (vertex.getId().equals(id)){
                    return vertex;
                }
            }
            return null;
        }

        /**
         * Traverse the graph breadth-first
         * Find path from Vertex a to Vertex b
         * If b is null method will return a list of all arcs needed to reach all reachable points in graph
         * source: https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
         * @param a source vertex
         * @param b destination vertex
         * @return String of traversed vertices
         * **/
        public List<Arc> breadthFirst(Vertex a, Vertex b) {
            if (getVertexList().isEmpty()) throw new RuntimeException("Graph does not contain any vertices");
            if (!getVertexList().contains(a)) {
                throw new RuntimeException("Graph does not contain given source vertex");
            } else if (!getVertexList().contains(b) && b != null){
                throw new RuntimeException("Graph does not contain given destination vertex");
            }

            List<Vertex> vertices = getVertexList();
            for (Vertex vertex : vertices) {
                vertex.setInfo(0);
            }

            boolean found = false;
            List<Vertex> vertexQueue = Collections.synchronizedList(new LinkedList<>());
            List<Arc> visited = new LinkedList<>();
            vertexQueue.add(a);
            a.setInfo(1);

            while (vertexQueue.size() > 0){
                Vertex v = vertexQueue.remove(0);
                List<Arc> fromCurrentVertex = v.getArcs();
                for (Arc arc : fromCurrentVertex) {
                    Vertex target = arc.getTargetVertex();
                    if (target.info == 0){
                        visited.add(arc);
                        if (target.equals(b)){
                            found = true;
                            return visited;
                        }
                        vertexQueue.add(target);
                        target.setInfo(1);
                    }
                }
            }

            if (b != null && !found){
                throw new RuntimeException("Not possible to get from source " + a + " to destination " + b);
            }
            return visited;
        }

        /**
         * Create a graph from the original graph where a triangle from the original
         * graph forms one new vertex in the new graph. The vertexes in the new graph
         * are connected if 2 of the 3 original graph vertexes that it was created
         * from are the same.
         * @return triangle graph.
         */
        public Graph createTriangleGraph() {
            int[][] graphMatrix = createAdjMatrix();
            List<Vertex> triangleVertexes = new ArrayList<>();
            for (int i = 0; i < graphMatrix.length; i++) {
                List<Integer> currentRow = new ArrayList<>();
                for (int j = i + 1; j < graphMatrix.length; j++) {
                    if (graphMatrix[i][j] == 1) {
                        currentRow.add(j);
                    }
                }
                findTriangles(graphMatrix, currentRow, i, triangleVertexes);
            }
            triangleVertexes = addArcsToTriangleVertexes(triangleVertexes);
            Graph triangleGraph = new Graph("triangle graph");
            if (triangleVertexes.size() > 0) {
                triangleGraph.addVertexesToEmptyGraph(triangleVertexes);
            }
            return triangleGraph;
        }

        /**
         * Finds, creates and adds triangle vertexes from the matrix and current
         * vertex's list of vertexes he has a common edge with. If the current vertex
         * has atleast 2 edges, then it means there can be a triangle formed by those
         * vertexes. Looping through all the vertex numbers (currentRow) looping every
         * single one through the same list starting from the one to their right(so
         * there wouldn't be triangles that are already made from the 3 vertexes made
         * again) and checking if in the matrix that vertex (in the place j) has a
         * connection(1) in the place of the current vertex number (k), if it does
         * than that means there is a triangle connection in-between those 3(including
         * the one in place i) vertexes which are placed in the matrix spots i,j,k. and
         * then we create a new vertex and combine the numbers i,j,k to make its name and
         * also add those 3 numbers as a list to the vertex "vertexes" so we can use them
         * to create the connections between the triangle vertexes later on
         * @param graphMatrix adj matrix of the graph given
         * @param currentRow list of numbers which the current vertex has a common edge with.
         * @param i number of the Vertex currently being looked at
         * @param triangleVertexes list of vertexes which were formed by triangles in the graph given.
         */
        public void findTriangles(int[][] graphMatrix, List<Integer> currentRow, int i, List<Vertex> triangleVertexes) {
            int currentVertexEdgeCounter = currentRow.size();
            if (currentVertexEdgeCounter >= 2) {
                for (int j = 0; j < currentVertexEdgeCounter; j++) {
                    for (int k = j + 1; k < currentVertexEdgeCounter; k++) {
                        if (graphMatrix[currentRow.get(j)][currentRow.get(k)] == 1) {
                            Vertex triangleVertex = new Vertex("v" + i + "v" + currentRow.get(j) + "v" + currentRow.get(k));
                            triangleVertex.vertexes = Arrays.asList(i, currentRow.get(j), currentRow.get(k));
                            triangleVertexes.add(triangleVertex);
                        }
                    }
                }
            }
        }

        /**
         * Compare every triangle vertex to all the other ones that come after them in the triangleVertex list and check
         * their "vertexes", if 2 of the vertexes they were created from were the same, then we create a connection(edge)
         * between the two triangle vertexes with 2 arcs, one from a to b other from b to a.
         * @param triangleVertexes list of vertexes created from triangle's.
         * @return triangleVertexes list where all the vertexes have the proper arcs
         */
        public List<Vertex> addArcsToTriangleVertexes(List<Vertex> triangleVertexes) {
            for (int i = 0; i < triangleVertexes.size(); i++) {
                Vertex currentVertex =  triangleVertexes.get(i);
                for (int j = i + 1; j < triangleVertexes.size(); j++) {
                    int inCommonVertexes = 0;
                    Vertex vertexToCompareTo = triangleVertexes.get(j);
                    for (Integer k : currentVertex.vertexes) {
                        if (vertexToCompareTo.vertexes.contains(k)) {
                            inCommonVertexes++;
                        }
                    }
                    if (inCommonVertexes == 2) {
                        createArc("a" + currentVertex.id + "_" + vertexToCompareTo.id, currentVertex, vertexToCompareTo);
                        createArc("a" + vertexToCompareTo.id + "_" + currentVertex.id, vertexToCompareTo, currentVertex);
                    }
                }
            }
            return triangleVertexes;
        }
        /**
         * This method takes a list of vertexes and adds them to a empty graph.
         * @param vertexList List of vertexes.
         */
        public void addVertexesToEmptyGraph(List<Vertex> vertexList) {
            first = vertexList.get(0);
            for (int i = 1; i < vertexList.size(); i++) {
                Vertex currentVertex = vertexList.get(i);
                currentVertex.next = first;
                first = currentVertex;
            }
        }
        /**
         * Create a complement graph for the original graph.
         * <p>
         * Original graph vertices keep their names.
         * Arcs will get a default name which has the following format:
         * "<FROM_VERTEX_ID>_<TO_VERTEX_ID>" (without quotes)
         *
         * @return complement graph
         */
        public Graph getComplementGraph() {
            Graph complementGraph = new Graph(id + "'");

            // Invert the original adjacency matrix.
            int[][] adjacencyMatrix = createAdjMatrix();

            for (int row = 0; row < adjacencyMatrix.length; row++) {
                for (int column = 0; column < adjacencyMatrix.length; column++) {
                    if (row == column) {
                        adjacencyMatrix[row][column] = 0;
                        continue;
                    }

                    adjacencyMatrix[row][column] = adjacencyMatrix[row][column] != 0 ? 0 : 1;
                }
            }

            // Deep copy the original vertices. First arcs are null,
            // because only vertices are needed from the original graph.
            Vertex[] vertices = new Vertex[adjacencyMatrix.length];

            complementGraph.first = first != null ? new Vertex(first.id, first.next, null) : null;

            Vertex vertex = complementGraph.first;
            int index = 0;
            while (vertex != null) {
                vertices[index++] = vertex;

                if (vertex.next != null) {
                    vertex.next = new Vertex(vertex.next.id, vertex.next.next, null);
                }

                vertex = vertex.next;
            }

            // Create edges between vertices based on the inverted adjacency
            // matrix.
            for (int row = index - 1; row >= 0; row--) {
                for (int column = index - 1; column >= 0; column--) {
                    boolean createArc = adjacencyMatrix[row][column] == 1;

                    if (createArc) {
                        Vertex from = vertices[row];
                        Vertex to = vertices[column];

                        complementGraph.createArc(from.id + "_" + to.id, from, to);
                    }
                }
            }

            return complementGraph;
        }
    }
}